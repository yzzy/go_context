package main

import (
	"context"
	"fmt"
)

const RequestID = "requestID"

func main() {
	ctx := context.Background()
	SendContext(ctx)
}

type CtxSendKey string

func SendContext(ctx context.Context) {
	key := CtxSendKey(RequestID)
	ctx = context.WithValue(ctx, key, "123")
	ReciverContext(ctx)
}

type CtxReciverKey string

func ReciverContext(ctx context.Context) {
	key := CtxReciverKey(RequestID)
	ctx = context.WithValue(ctx, key, "3434")
	LoggerContext(ctx)
}

func LoggerContext(ctx context.Context) {
	fmt.Println("Send", CtxSendKey(RequestID), ctx.Value(CtxSendKey(RequestID)))
	fmt.Println("Receiver", CtxReciverKey(RequestID), ctx.Value(CtxReciverKey(RequestID)))
}
