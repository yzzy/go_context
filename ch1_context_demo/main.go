package main

import (
	"context"
	"fmt"
	"log"
	"time"
)

const UserName = "username"

// func  context fetchHouseInfo
// third context houseInChinaHTTPCall
type ContextString string

// should not use built-in type string as key for value; define your own type to avoid collisions (SA1029)
func main() {
	start := time.Now()
	ctx := context.WithValue(context.Background(), ContextString(UserName), "yzzy") // root context
	houseId, price, err := fetchHouseInfo(ctx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("House info response: use time %v -> %v price: %v\n", time.Since(start), houseId, price)
}

// 请求 houseInChina 然后处理结果
func fetchHouseInfo(ctx context.Context) (string, int, error) {
	//cancel deadline
	ctx, cancel := context.WithTimeout(ctx, time.Millisecond*900)
	// 如果返回是权限未授权 主动cancel 调用cancel()
	defer cancel()

	type Result struct {
		houseId string
		price   int
		err     error
	}

	resultCh := make(chan Result, 1)

	value := ctx.Value(ContextString(UserName))
	fmt.Println(value)

	// 调用httpclient匿名函数
	go func() {
		res, price, err := houseInChinaHTTPCall()
		resultCh <- Result{houseId: res, price: price, err: err}
	}()
	// context.Done
	select {
	// Done
	// 1. time out
	// 2. cancel主动调用
	case <-ctx.Done():
		return "", 0, fmt.Errorf("error happen %v", time.Now())
	case res := <-resultCh:
		return res.houseId, res.price, res.err
	}
}

// httpclient 模拟
func houseInChinaHTTPCall() (string, int, error) {
	time.Sleep(time.Millisecond * 1000)
	return "house id 1", 100000, nil
}
