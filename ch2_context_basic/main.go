package main

import (
	"context"
	"fmt"
)

// type todoCtx struct{ emptyCtx }
// type backgroundCtx struct{ emptyCtx }
// todoCtxEx != backgroundCtxEx
func main() {
	// background vs todo
	ctx := context.Background()
	fmt.Printf("%s\n", ctx)
	fmt.Printf("\tDone: \t%#v\n", ctx.Done())
	fmt.Printf("\tErr: \t%#v\n", ctx.Err())
	fmt.Printf("\tValue: \t%#v\n", ctx.Value("key"))
	deadline, ok := ctx.Deadline()
	fmt.Printf("\tDeadline: \t%s(%t)\n", deadline, ok)
	// time.Time

	ctxTodo := context.TODO()
	fmt.Printf("%s\n", ctxTodo)
	fmt.Printf("\tDone: \t%#v\n", ctxTodo.Done())
	fmt.Printf("\tErr: \t%#v\n", ctxTodo.Err())
	fmt.Printf("\tValue: \t%#v\n", ctxTodo.Value("key"))
	deadline, ok = ctxTodo.Deadline()
	fmt.Printf("\tDeadline: \t%s(%t)\n", deadline, ok)
	// valueCtx{parent, key, val}
	ctx = context.WithValue(ctx, "key1", "value1") //{key1: value1}
	ctx = context.WithValue(ctx, "key1", "value2") // {*{key1:value1}, {key1:value2}}
	ctx = context.WithValue(ctx, "key1", "value3") // {{*{key1:value1}, {key1:value2}}, {key1: value3}}
	fmt.Println(ctx.Value("key1"))
	fmt.Printf("ctx: %#v\n", ctx)

	// ctx = context.WithValue(ctx, map[string]string{}, 23)
}

// func (emptyCtx) Deadline() (deadline time.Time, ok bool) {
// 	return
// }
